import datetime
import csv
import sys
import urllib.request

class AccelertionData():
    def __init__(self):
        self.site_code = ''
        self.site_position = (0, 0) # (lattitude, longitude)
        self.sampling_rate = 1 # Hz
        self.unit = 'gal'
        self.initial_time = 0
        self.sequence = [] # [(NS, EW, UD), ...]
    
    def print_meta_data(self):
        print('Site code:', self.site_code)
        print('Site position:', self.site_position)
        print('Sampling rate:', self.sampling_rate, 'Hz')
        print('Unit:', self.unit)
        print('Initial time:', self.initial_time)
        print('Sequence length:', len(self.sequence))

def read_jma_csv(url):
    with urllib.request.urlopen(url) as csv_file:
        lines = csv_file.read().splitlines()

        acceleration_data = AccelertionData()
        acceleration_data.site_code = lines[0].partition(b'=')[2].lstrip()[0:3].decode('utf-8')
        site_lat = float(lines[1].partition(b'=')[2])
        site_lon = float(lines[2].partition(b'=')[2])
        acceleration_data.site_position = (site_lat, site_lon)
        acceleration_data.sampling_rate = float(lines[3].partition(b'=')[2].lstrip().rstrip(b'Hz'))
        acceleration_data.unit = lines[4].partition(b'=')[2].lstrip().decode('utf-8')
        datetime_data = [int(token) for token in lines[5].partition(b'=')[2].split()]
        acceleration_data.initial_time = datetime.datetime(
            datetime_data[0], datetime_data[1], datetime_data[2],
            datetime_data[3], datetime_data[4], datetime_data[5])
        
        lines[6] = lines[6].lstrip()
        sequence_reader = csv.DictReader([byte_array.decode('utf-8') for byte_array in lines[6:]])

        for row in sequence_reader:
            acceleration_data.sequence.append((float(row['NS']), float(row['EW']), float(row['UD'])))
    
        acceleration_data.print_meta_data()

if __name__ == '__main__':
    url = sys.argv[1]
    read_jma_csv(url)
